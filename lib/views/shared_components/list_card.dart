import 'package:flutter/material.dart';
import 'package:sizing/sizing.dart';

class ListCard extends StatelessWidget {
  final String title;
  final String description;
  final String imageURL;
  final DateTime lastUpdated;
  final int starCount;
  final String language;
  final Function() onPressed;

  const ListCard(
      {super.key,
      required this.title,
      required this.description,
      required this.imageURL,
      required this.lastUpdated,
      required this.starCount,
      required this.language,
      required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            ListTile(
              leading: Image.network(
                imageURL,
                height: 30.ss,
              ),
              title: Text(title),
              subtitle: Text(description),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Padding(
                  padding: EdgeInsets.only(right: 0.06.sw),
                  child: Text(
                    "Last updated ${lastUpdated.difference(DateTime.now()).inDays} days ago",
                    style: const TextStyle(
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.italic),
                  ),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 20, bottom: 20),
                  child: Row(
                    children: [
                      Row(
                        children: [
                          Icon(
                            Icons.circle,
                            size: 20.ss,
                          ),
                          Text(
                            language,
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Icon(
                            Icons.star_border,
                            size: 20.ss,
                          ),
                          Text(
                            " ${_convertToKVersion(starCount)}",
                            style: const TextStyle(fontWeight: FontWeight.bold),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(right: 0.03.sw),
                      child: ElevatedButton(
                        onPressed: onPressed,
                        style: ButtonStyle(
                            backgroundColor:
                                MaterialStateProperty.all(Colors.purpleAccent)),
                        child: Row(
                          children: [
                            Icon(
                              Icons.open_in_new,
                              color: Colors.white,
                              size: 20.ss,
                            ),
                            SizedBox(
                              width: 0.01.sw,
                            ),
                            const Text(
                              "Details",
                              style: TextStyle(color: Colors.white),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  String _convertToKVersion(int number) {
    if (number < 1000) {
      return number.toString();
    } else {
      double result = number / 1000;
      return '${result.toStringAsFixed(1)}K';
    }
  }
}
