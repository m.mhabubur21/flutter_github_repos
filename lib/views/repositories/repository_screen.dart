import 'package:flutter/material.dart';
import 'package:flutter_github_repos/controllers/repository_controller.dart';
import 'package:flutter_github_repos/models/repo.dart';
import 'package:flutter_github_repos/views/repositories/single_repository.dart';
import 'package:flutter_github_repos/views/shared_components/list_card.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:sizing/sizing.dart';

class RepositoryScreen extends StatefulWidget {
  const RepositoryScreen({super.key});

  static String route = '/repository';

  @override
  State<RepositoryScreen> createState() => _RepositoryScreenState();
}

class _RepositoryScreenState extends State<RepositoryScreen> {
  RepositoryController repositoryController = RepositoryController();
  final PagingController<int, Repo> _pagingController =
      PagingController(firstPageKey: 0);

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) {
      fetchRepoPage(pageKey);
    });
    super.initState();
  }

  Future<List<Repo>> loadPage() async {
    List<Repo> data = await repositoryController.getFlutterRepos();
    return data;
  }

  Future<void> fetchRepoPage(int pageKey) async {
    try {
      int endIndex = repositoryController.displayCount + repositoryController.pageSize;
      endIndex = endIndex < repositoryController.allRepo.length
          ? endIndex
          : repositoryController.allRepo.length;

      List<Repo> newItems = repositoryController.allRepo
          .sublist(repositoryController.displayCount, endIndex);
      repositoryController.displayCount += repositoryController.pageSize;
      print(newItems);
      final isLastPage = newItems.length < repositoryController.pageSize && newItems.isNotEmpty;
      if (isLastPage) {
        _pagingController.appendLastPage(newItems);
      } else {
        final nextPageKey = pageKey + newItems.length;
        _pagingController.appendPage(newItems, nextPageKey);
      }
    } catch (error) {
      _pagingController.error = error;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const Padding(
          padding: EdgeInsets.all(8.0),
          child: FlutterLogo(),
        ),
        title: const Text("Flutter Github Repos"),
      ),
      body: FutureBuilder<List<Repo>>(
          future: loadPage(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        const Text("Sort By:"),
                        SizedBox(
                          width: 0.02.sw,
                        ),
                        DropdownButton(
                            items: RepositoryController.sortingTypes()
                                .map((String item) => DropdownMenuItem<String>(
                                      value: item,
                                      child: Text(
                                        item,
                                        style: const TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ))
                                .toList(),
                            value: repositoryController.selectedValue,
                            onChanged: (String? value) {
                              setState(() {
                                repositoryController.selectedValue =
                                    value ?? repositoryController.selectedValue;
                              });
                            }),
                      ],
                    ),
                  ),
                  Expanded(
                    child: SizedBox(
                      height: 300,
                      child: PagedListView<int, Repo>(
                        pagingController: _pagingController,
                        builderDelegate: PagedChildBuilderDelegate<Repo>(
                          itemBuilder: (context, item, index) => ListCard(
                            title: item.fullName,
                            description: item.description,
                            imageURL: item.owner.avatarUrl,
                            lastUpdated: item.updatedAt!,
                            starCount: item.stargazersCount ?? 0,
                            language: item.language ?? "None",
                            onPressed: () {
                              showDialog(context: context, builder: (BuildContext context) {
                                return SingleRepository(repo: item,);
                              });
                            },
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              );
            } else if (snapshot.hasError) {
              return Center(
                child: Text(snapshot.error.toString()),
              );
            } else {
              return const Center(child: CircularProgressIndicator());
            }
          }),
    );
  }
}
