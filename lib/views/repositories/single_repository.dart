import 'package:flutter/material.dart';
import 'package:flutter_github_repos/models/repo.dart';
import 'package:sizing/sizing.dart';
import 'package:intl/intl.dart';


class SingleRepository extends StatelessWidget {
  final Repo repo;
  const SingleRepository({super.key, required this.repo});

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Row(
        children: [
          Image.network(
            repo.owner.avatarUrl,
            height: 30.ss,
          ),
          SizedBox(
            width: 0.02.sw,
          ),
          Text(repo.owner.login),
        ],
      ),
      content: SizedBox(
        height: 0.2.sh,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Description: ${repo.description}'),
              SizedBox(
                height: 0.02.sh,
              ),
              Text(
                  'Last updated: ${DateFormat('dd-MM-yy HH:mm').format(repo.updatedAt!)}'),
            ],
          ),
        ),
      ),
      actions: [
        TextButton(
          onPressed: () {
            Navigator.of(context).pop(); // Close the dialog
          },
          child: Text('Close'),
        ),
      ],
    );
  }
}
