class License {
  final String? key;
  final String nodeId;
  final String name;
  final String? spdxId;
  final String? url;

  License({
    required this.nodeId,
    required this.name,
    this.key,
    this.spdxId,
    this.url,
  });

  factory License.fromJson(Map<String, dynamic> json) {
    return License(
      key: json['key'],
      name: json['name'],
      spdxId: json['spdx_id'],
      url: json['url'],
      nodeId: json['node_id'],
    );
  }
}
