import 'dart:ffi';

import 'package:flutter_github_repos/models/license.dart';
import 'package:flutter_github_repos/models/owner.dart';

class Repo {
  final int id;
  final String? nodeId;
  final String name;
  final String fullName;
  final bool? private;
  final Owner owner;
  final String htmlUrl;
  final String description;
  final int? stargazersCount;
  final String? language;
  final double? score;
  final DateTime? updatedAt;

  Repo({
    required this.id,
    this.nodeId,
    required this.name,
    required this.fullName,
    this.private,
    required this.owner,
    required this.htmlUrl,
    required this.description,
    this.stargazersCount,
    this.language,
    required this.score,
    this.updatedAt
  });

  factory Repo.fromJson(Map<String, dynamic> json) {
    print(json["updated_at"]);
    return Repo(
      id: json['id'],
      nodeId: json['node_id'],
      name: json['name'],
      fullName: json['full_name'],
      private: json['private'],
      owner: Owner.fromJson(json['owner']),
      htmlUrl: json['html_url'],
      description: json['description'] ?? "",
      stargazersCount: json['stargazers_count'],
      score: json['score'],
      updatedAt: DateTime.tryParse(json["updated_at"])
    );
  }
}
