import 'package:flutter_github_repos/models/repo.dart';
import 'package:flutter_github_repos/services/api_services.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';

class RepositoryController {
  int pageSize = 10;
  int _totalCount = 0;
  int displayCount = 0;
  late List<Repo> allRepo = [];
  String selectedValue = sortingTypes().first;

  ApiServices apiServices = ApiServices();

  Future<List<Repo>> getFlutterRepos() async {
    List<Repo> repos = [];
    try {
      Map<String, dynamic> response = await apiServices.getRequest(
          "https://api.github.com/search/repositories?q=Flutter", null);
      _totalCount = response["total_count"];
      List<dynamic> data = response["items"];
      repos =
          data.where((e) => e != null)
          .map((rawData) => Repo.fromJson(rawData))
          .toList();
    } catch (e) {
      throw Exception("Unable to convert fetched data\n$e");
    }
    allRepo = repos;
    return repos;
  }

  static List<String> sortingTypes() {
    return ["Last Updated", "Star Count"];
  }
}
