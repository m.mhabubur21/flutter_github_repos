import 'dart:convert';
import 'package:http/http.dart' as http;

class ApiServices {
  Map<String, String> headersWithToken(String token) {
    return {
      'Authorization': 'Bearer $token',
      'Content-Type': 'application/json',
    };
  }

  Future<Map<String, dynamic>> postRequest(String url,
      Map<String, dynamic> body, Map<String, String> headers) async {
    final response = await http.post(Uri.parse(url),
        headers: headers, body: jsonEncode(body));

    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    }
    throw Exception(jsonDecode(response.body)["message"]);
  }

  Future<Map<String, dynamic>> getRequest(
    String url,
    Map<String, String>? headers, {
    Map<String, dynamic>? queryParams,
  }) async {
    final response = await http.get(
        Uri.parse(url).replace(queryParameters: queryParams),
        headers: headers);
    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    }
    throw Exception(jsonDecode(response.body)["message"]);
  }

  Future<Map<String, dynamic>> putRequest(
    String url,
    Map<String, String>? headers,
    Map<String, dynamic> requestBody,
  ) async {
    final response = await http.put(
      Uri.parse(url),
      headers: headers,
      body: jsonEncode(requestBody),
    );

    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    }
    throw Exception(jsonDecode(response.body)["message"]);
  }

  Future<Map<String, dynamic>> deleteRequest(
    String url,
    Map<String, String>? headers, {
    Map<String, dynamic>? queryParams,
  }) async {
    final response = await http.delete(
      Uri.parse(url).replace(queryParameters: queryParams),
      headers: headers,
    );

    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    }
    throw Exception(jsonDecode(response.body)["message"]);
  }
}
