import 'package:flutter/material.dart';
import 'package:flutter_github_repos/views/repositories/repository_screen.dart';


final Map<String, WidgetBuilder> routes = {
  RepositoryScreen.route : (context) => const RepositoryScreen()
};
