import 'package:flutter/material.dart';
import 'package:sizing/sizing.dart';
import 'package:flutter_github_repos/routes.dart';
import 'package:flutter_github_repos/views/repositories/repository_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return SizingBuilder(
        builder: () => MaterialApp(
              title: 'Github Flutter Repos',
              debugShowCheckedModeBanner: false,
              theme: ThemeData(
                colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
                useMaterial3: true,
              ),
              routes: routes,
              initialRoute: RepositoryScreen.route,
            ));
  }
}
